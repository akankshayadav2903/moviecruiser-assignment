var movies=[];
var favourites=[];
function getMovies(){
	var xhr = new XMLHttpRequest();
	xhr.open("get","http://localhost:3000/movies");
	xhr.send(null)
	xhr.onload=function(){
		movies=JSON.parse(xhr.responseText);
		console.log(movies);
		for(let i=0;i<movies.length;i++){
			document.getElementById("moviesList").innerHTML+=
			`<div class="product">
			<div><h6>${movies[i].title}</h6></div>
			<img src=${movies[i].posterpath}></img><br><br>
			<button type="button" class="btn btn-primary" onclick="addFavourites(${movies[i].id})">add to favorites</button>
			</div><br>`
		}
	}
}

function getFavourites(){
	var xhr = new XMLHttpRequest();
	console.log("GetF")
	console.log(xhr.readyState);
	xhr.open("get","http://localhost:3000/favourites");
	xhr.send(null)
	xhr.onload=function(){
		favourites=JSON.parse(xhr.responseText);
		console.log("F"+ favourites);
		for(let i=0;i<favourites.length;i++){
			document.getElementById("favouritesList").innerHTML+=
			`<div class="fav">
			<div><h6>${favourites[i].title}</h6></div>
			<img src="${favourites[i].posterpath}" alt="${favourites[i].title}"></img><br><br>
			<button type="button" class="btn btn-primary" onclick="deleteFavourites(${favourites[i].id})">Delete</button>
			</div><br>`
		}
	}
}

function addFavourites(id){
	console.log("added to favourites");
	let obj=getMovieInfoById(id)

	return fetch("http://localhost:3000/favourites",
	{
		method:'POST',
		body:JSON.stringify(obj),
		headers:{
			"Content-type":"application/json"
		}

	})
	.then(res=>res.json())
	.then(json => console.log(id))
}

const getMovieInfoById=(moviesId)=>{
	for(let i=0;i<movies.length;i++){
		if(moviesId==movies[i].id){
			console.log(movies[i])
			return movies[i];

		}
	}

}

const deleteFavourites=(favId)=>
{    
	console.log("Delete working");
	console.log(`Movie id= ${favId} deleted`);
	return fetch (`http://localhost:3000/favourites/${favId}`,
	{
		method:'DELETE',
		headers:{"Content-type":"application/json"}
	})
}

module.exports={
	getMovies,
	getFavourites,
	addFavourites,
	deleteFavourites
};